Hosting Cloud Files provides integration with Rackspace Cloud Files and Aegir.
New Rackspace Cloud Files containers will be created for Aegir sites that
enable this functionality. In addition, the site's file system settings are set
to use Rackspace by default. Global Cloud Files settings can be configured in
Aegir by accessing 'admin/hosting/cloud_files'.

Installation

Download Hosting Cloud Files to sites/all/modules
Download http://php-opencloud.com/ and extract to sites/all/libraries
  'sites/all/libraries/php-opencloud/vendor/autoload.php' should exist to
  load the library).
Enable Hosting Cloud Files
Configure the Cloud Files api key and username in 'admin/hosting/cloud_files'
Select "Yes" to enable Hosting Cloud Files while creating/editing a site.

Requirements

Aegir 3.x (2.x support will be released at a later date)
Provision Cloud Files
Cloud Files module on site's platform
  (may need to be required as a dependency in install profile info file.)

Resources

Rackspace Cloud Files   - http://www.rackspace.com/cloud/files/
Aegir                   - https://drupal.org/project/hostmaster
Provision Cloud Files   - https://drupal.org/node/2283981
php-opencloud           - http://php-opencloud.com/
Cloud Files             - https://drupal.org/project/cloud_files
