<?php

/**
 * @file
 * Admin UI code for Cloud Files support.
 */

/**
 * General settings form.
 */
function hosting_cloud_files_settings_form() {
  $form = array();

  $form['hosting_cloud_files_username'] = array(
    '#type' => 'textfield',
    '#title' => 'Rackspace API Username',
    '#default_value' => variable_get('hosting_cloud_files_username', ''),
  );

  $form['hosting_cloud_files_api_key'] = array(
    '#type' => 'textfield',
    '#title' => 'Rackspace API Key',
    '#default_value' => variable_get('hosting_cloud_files_api_key', ''),
  );

  $form['hosting_cloud_files_region'] = array(
    '#type' => 'select',
    '#title' => t('Rackspace Cloud Files Region'),
    '#options' => array(
      HOSTING_CLOUD_FILES_RACKSPACE_REGION_CHICAGO => t('Chicago'),
      HOSTING_CLOUD_FILES_RACKSPACE_REGION_DALLAS => t('Dallas/Ft. Worth'),
      HOSTING_CLOUD_FILES_RACKSPACE_REGION_HONG_KONG => t('Hong Kong'),
      HOSTING_CLOUD_FILES_RACKSPACE_REGION_LONDON => t('London'),
      HOSTING_CLOUD_FILES_RACKSPACE_REGION_VIRGINIA => t('Northern Virginia'),
      HOSTING_CLOUD_FILES_RACKSPACE_REGION_SYDNEY => t('Sydney'),
    ),
    '#default_value' => variable_get('hosting_cloud_files_region', HOSTING_CLOUD_FILES_RACKSPACE_DEFAULT_REGION),
  );

  $form['hosting_cloud_files']['status'] = array(
    '#type' => 'radios',
    '#title' => 'Enable cloud files by default on new sites',
    '#options' => array(
      HOSTING_CLOUD_FILES_SITE_DISABLED => t('No'),
      HOSTING_CLOUD_FILES_SITE_ENABLED => t('Yes'),
    ),
    '#default_value' => variable_get('hosting_cloud_files_default_status', HOSTING_CLOUD_FILES_SITE_DISABLED),
  );

  return system_settings_form($form);
}
