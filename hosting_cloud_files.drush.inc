<?php

/**
 * @file
 * Provision/drush hooks for providing context and provisioning drupal config.
 */

/**
 * Implements drush_hook_pre_hosting_task().
 */
function drush_hosting_cloud_files_pre_hosting_task() {
  $task =& drush_get_context('HOSTING_TASK');

  // Load data for the site status and cdn from database.
  $data = db_query('SELECT * FROM {hosting_cloud_files} WHERE nid = :nid',
    array(':nid' => $task->rid))->fetchObject();

  // Bring in cloud files system settings.
  if ($task->task_type === 'verify') {
    $task->options['cloud_files_status'] = $data->status;
    $task->options['cloud_files_cdn_domain'] = $data->cdn_domain;
    $task->options['cloud_files_scheme'] = variable_get('hosting_cloud_files_scheme', HOSTING_CLOUD_FILES_RACKSPACE_DEFAULT_SCHEME);
    $task->options['cloud_files_region'] = variable_get('hosting_cloud_files_region', HOSTING_CLOUD_FILES_RACKSPACE_DEFAULT_REGION);
    $task->options['cloud_files_username'] = variable_get('hosting_cloud_files_username', '');
    $task->options['cloud_files_api_key'] = variable_get('hosting_cloud_files_api_key', '');
  }
}
